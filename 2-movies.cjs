const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/


// Q1. Find all the movies with total earnings more than $500M. 

function totalEarningMovies() {

    const dataOfMovies = Object.entries(favouritesMovies);
    const moviesData = dataOfMovies.filter((data) => {

        return data[1].totalEarnings.slice(1, 4) > 500;


    })

    console.log(moviesData);

}
totalEarningMovies();


// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.


function totalOscarMovies() {

    const dataOfMovies = Object.entries(favouritesMovies);
    const moviesData = dataOfMovies.filter((data) => {

        return (data[1].totalEarnings.slice(1, 4) > 500 && data[1].oscarNominations > 3);

    })

    console.log(moviesData);


}
totalOscarMovies();



// Q.3 Find all movies of the actor "Leonardo Dicaprio".


function moviesofLeaonardo() {

    const dataOfMovies = Object.entries(favouritesMovies);
    const LeaonardoMovies = dataOfMovies.filter((data) => {

        return data[1].actors.includes("Leonardo Dicaprio");

    })

    console.log(LeaonardoMovies);

}
moviesofLeaonardo();



// Q.4 Sort movies (based on IMDB rating)


function sortMovies() {

    const sortMoviesData = Object.keys(favouritesMovies).sort((key1, key2) => {

        const data1 = favouritesMovies[key1];
        const data2 = favouritesMovies[key2];

        const imdb1 = data1.imdbRating;
        const imdb2 = data2.imdbRating;

        const earningMovies1 = Number(data1.totalEarnings.slice(1, 4));
        const earningMovies2 = Number(data2.totalEarnings.slice(1, 4));


        if (imdb1 > imdb2) {
            return 1;
        }
        else if (imdb1 < imdb2) {
            return -1;
        }
        else {

            if (earningMovies1 > earningMovies2) {
                return 1;
            }
            else if (earningMovies1 < earningMovies2) {
                return -1;
            }
            else {
                return 0;
            }

        }

    });

    const res = sortMoviesData.map(key => {
        return [key, favouritesMovies[key]];
    });

     
    console.log(Object.fromEntries(res));
}
sortMovies();



// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:


function groupMoviesOnGeners() {

    const priorityGenres = ["drama", "sci-fi", "adventure", "thriller", "crime"];


    const groupedMovies = Object.entries(favouritesMovies).reduce((acc, [movie, details]) => {

        const movieGenres = details.genre;

        const genre = priorityGenres.find((genre) => {
            movieGenres.includes(genre)
        });

        if (genre) {
            if (!acc[genre]) {
                acc[genre] = [];
            }

            acc[genre].push(movie, details);

        }

        return acc;
    }, {});

    console.log(groupedMovies);

}

groupMoviesOnGeners();

